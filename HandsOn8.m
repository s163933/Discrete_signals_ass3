% Hands on 8
% Asger

if((exist("Toolbox", 'dir')) == 7)
    addpath("Toolbox")
elseif (exist("Discrete_Signals_Ass2/Toolbox",'dir') == 7)
    addpath("Discrete_Signals_Ass2/Toolbox");
end

%% Bandpass
% Pass: 0.2 - 0.3
% Stops: 0 - 0.1,  0.4 - 1
% Ripples max 2dB
% Stop is min -100 dB

Wp = [0.2 0.3];
Ws = [0.1 0.4];
Rp = 2; %dB
Rs = 100; %dB

[n, Wn] = buttord(Wp, Ws, Rp, Rs);
[z,p,k] = butter(n, Wn, 'bandpass');

sos = zp2sos(z,p,k);

figure(1)
freqz(sos)
title(sprintf("Frequency response of order %d bandpass filter, %s = [%.3f %.3f], ",...
    2*n, "\omega_p", Wn(1), Wn(2)));
handle = gca;
handle2 = handle.Parent.Children(2); % Hack, but it works
set(handle, 'FontSize', 13)
set(handle2, 'FontSize', 13)
ylim(handle,[-60 5]);
saveas(gcf, 'FrqRespBandpass.eps', 'epsc');

figure(2)
zplane(z,p)
title(sprintf("Zero-pole plot of order %d bandpass filter,\nwith: %s = [%.3f %.3f]",...
    2*n, "\omega_p", Wn(1), Wn(2)));
set(gca, 'FontSize', 13)
saveas(gcf, 'ZPBandpass.eps', 'epsc');

%%
figure(3)
[resp, t] = impz(sos);
plot(t,resp);

%%
rect = true;
% rect = false;
highest = max(resp);
cut_ps = [0.1 0.4 0.6 0.75 1];

% Find when resp is <10% max value
ps = 0.1;
limit = highest * ps;
above_index = resp > limit;
start_i = find(above_index, 1);
end_i = find(above_index, 1, 'last');

cut_resps = [];
figure(4)
for ps = cut_ps
    if rect
        cut_resp = resp(1:round(ps*end_i));
    else
        curve = zeros(1,length(resp));
        w_L = round(ps*end_i);
        curve(1:w_L) = hann(w_L);
        cut_resp = resp .* curve';
    end
    freqz(cut_resp, 1);
    handle = gca;
%     set(handle, 'FontSize', 13);
%     set(handle.Parent.Children(2), 'FontSize', 13); 
    hold on
    hold(handle.Parent.Children(2), 'on');
end
% freqz(resp,1);
hold off
hold(handle.Parent.Children(2), 'off');
% handle = gca;
set(handle, 'FontSize', 13);
set(handle.Parent.Children(2), 'FontSize', 13); 
ylim([-30 5])
legend("10%", "40%","60%","75%","100%")
% Color the figure
c_map = [1 0 1; 0 1 1; 1 0 0; 0 1 0; 0 0 1];
for k = 1:5
    handle.Children(k).Color = c_map(k,:);
    handle.Parent.Children(3).Children(k).Color = c_map(k,:);
end
if rect
    title("Frequency response of filters shortened with rect")
    saveas(gcf, 'FrqRespShortImpRespRect.eps', 'epsc');
else
    title("Frequency response of filters shortened with Hann")
    saveas(gcf, 'FrqRespShortImpRespHann.eps', 'epsc');
end

%% Part 2

% Bandpass cutoff at normalized 0.3, 0.6
% Gain 5 dB
% Length 900
l = 900;
[b,a] = bassandtreb([0.1 0.2], 30, l);

figure(7)
plot(b)
title("Impulse response of filter");
figure(8)
freqz(b,a);
%s_b = circshift(b,l/2);
% figure(30)
% freqz(s_b,a);

%% Mult with hann

curve = zeros(1,l);
w_L = l;
curve(1:w_L) = hann(w_L);

new_b = b .* curve;
s_b = circshift(b,l/2);
new_s_b = s_b .* curve;

figure(12)
plot(b)
hold on
plot(new_b)
% plot(curve)
plot(s_b)
plot(new_s_b)
hold off
legend("b", "new_b", "s_b", "new_s_b")

figure(20)
plot(s_b)
hold on
plot(new_s_b)
hold off
legend("Shifted bandpass", "Shifted bandpass with Hann windowing");
title("Shifted impulse response of bandpass filters");

figure(21)
plot(s_b)
hold on
plot(new_s_b)
hold off
legend("Shifted bandpass", "Shifted bandpass with Hann windowing");
title("Zoom of shifted impulse response of bandpass filters");
xlim([150 300])
ylim()

figure(13)
freqz(new_s_b, a)
title("Frequency response of shifted bandpass filter multiplied with Hann window")
figure(14)
freqz(s_b, a)
title("Frequency response of shifted bandpass filter")

%% test filter
% [sentence, fs_s] = audioread("spoken_sentence.wav");
% [piano, fs_p] = audioread("piano.wav");

% piano_f = filter(new_s_b, a, piano);
% sentence_f = filter(new_s_b, a, sentence);
% piano_f = piano_f/max(piano_f);
% sound(piano_f, fs_s)

%% 1.2 - Mostly from learn

% All f = [0 5k] Hz has gain 0dB
% fs+n unreadable
% Cutoff = 1/3 normalized frq = 1/3 Nf
% length(impz) = 601
% Nf = 3*5 kHz

% I guess fs = 2*Nf = 2*3*5 kHz
fs = 2*3*5*1000; % [Hz]
% Ts = 1/Ts;
n = 601;

% Design optimal frq response
w = 0:1/(n/2):1;

% Negative start
Y = zeros(size(w));
% Positive frequencies
Y(w <= 0.3) = 1;

% TODO: The same but in bassandtreb.m
temp = conj(Y(end:-1:2));
Y = [Y, temp];

% BEPP: Let's look at the spectrum as a whole.
w_norm = [w, -w(end:-1:2)];

% BEPP: Element 301 is the last one on the upper half of the unit circle,
% element 3002 the first one on the lower half, running backwards towarsds
% zero.
figure(14)
stem(w_norm,Y)
xlabel('normalized frequency')
ylabel('gain')
ylim([0 1.05*max(abs(Y))])

% BEPP: NOTE: In above plot the square is centered (because the sign in the
% w vecotor flips - the values in the Y vector are however starting from
% frequency 0.

% Set a frequency vector for plotting
% Plot spectrum
figure(16);
plot(w_norm,abs(Y),'linewidth',2);
title('Spectrum for positive and negative frequencies');
ylabel('Magnitude [a.u]');
xlabel('Normalized Frequency (\times \pi rad/sample)');

% Transform to time domain
y = ifft(Y);
% BEPP: Now we want to shift it in order to make it causal
y = circshift(y,floor(n/2));

figure(18)
plot(y)
set(gca, 'FontSize', 13);
xlim([0 length(y)]);
title(sprintf("Impulse response of lowpass filter\ndesigned from ideal filter"));
xlabel("Samples");
ylabel("Amplitude");
saveas(gcf, "impRespIdeal.eps", 'epsc');

%% Fuuuq

figure(15)
freqz(y,1)
ylim([-20 2]);

impulse_length = [601 301 101 51 21 11 7];

for il = impulse_length
    cut_imp = y(301-floor(il/2):301+floor(il/2));
    figure(4)
    freqz(cut_imp, 1);
    handle = gca;
%     set(handle, 'FontSize', 13);
%     set(handle.Parent.Children(2), 'FontSize', 13); 
    hold on
    hold(handle.Parent.Children(2), 'on');
end
% freqz(resp,1);
hold off
hold(handle.Parent.Children(2), 'off');
% handle = gca;
set(handle, 'FontSize', 13);
set(handle.Parent.Children(2), 'FontSize', 13); 
ylim([-30 5])
leg = [];
for k = 1:length(impulse_length)
    leg = [leg "n = "];
end
leg = leg + impulse_length;
legend(leg)
% Color the figure
c_map = [1 0 1; 0 1 1; 1 0 0; 0 1 0; 0 0 1; 0 0 0; 0 0.5 0.5];
for k = 1:length(impulse_length)
    handle.Children(k).Color = c_map(k,:);
    handle.Parent.Children(3).Children(k).Color = c_map(k,:);
end
title(sprintf("Frequency responses of lowpass filters\nwith shortened impulse response"))
saveas(gcf, 'FrqRespShortImpIdeal.eps', 'epsc');


