if((exist("Toolbox", 'dir')) == 7)
    addpath("Toolbox")
elseif (exist("Discrete_Signals_Ass2/Toolbox",'dir') == 7)
    addpath("Discrete_Signals_Ass2/Toolbox");
end

% Testing initEqualizer.m
gains = [0 -5 -10 -15 -20];
[b,a] = initEqualizer(gains);

figure(1)
plot(b)
title("Impulse response of filter");
xlabel("Samples");
ylabel("Ampliitude");
xlim([0 600]);
grid
set(gca, 'FontSize', 15)
saveas(gcf, 'ImpRespEqualizer.eps', 'epsc');

figure(2)
freqz(b,a);
% Make the figure nice
title("Frequency response of filter");
handle = gca;
handle2 = handle.Parent.Children(2); % Hack, but it works
set(handle, 'FontSize', 13)
set(handle2, 'FontSize', 13)
saveas(gcf, 'FrqRespEqualizer.eps', 'epsc');

% % Zero pad for fun

% zb = [b zeros(1,length(b)*9)];
% figure(3)
% plot(zb)
% title("Impulse response of zero padded filter");
% % saveas(gcf, 'FrequencyResponseWhiteNoise.eps', 'epsc');
% 
% figure(4)
% freqz(zb,a);
% title("Frequency response of zero padded filter");
% % saveas(gcf, 'FrequencyResponseWhiteNoise.eps', 'epsc');

%% Pass a white noise through
fs = 14000;
T_burst = 2;
T_total = T_burst*3;
t = 0:1/fs:T_total-1/fs;
sig = [zeros(fs*T_burst,1); randn(fs*T_burst,1); zeros(fs*T_burst,1)];
sig = sig/max(sig);
sig_filtered = filter(b,a,sig);
% % Not normalized
% sig_filtered = sig_filtered/max(sig_filtered);
delta_f = fs/length(sig);
freq = -fs/2:delta_f: fs/2-delta_f;
freq = freq(:);


figure(5)
plot(t,sig);
hold on
plot(t, sig_filtered);
hold off
title("Amplitude of a white noise signal in time");
legend("Unfiltered", "Filtered with equalizer");
xlabel("Time [s]");
ylabel("Amplitude");
set(gca, 'FontSize', 15)
xlim([T_burst-0.05 T_burst*2+0.05])
ylim([-1 1])
grid
saveas(gcf, 'WhiteNoiseInTime.eps', 'epsc');

% Big phase shift huh

figure(6)
fft_sig = fft(sig);
fft_sig_f = fft(sig_filtered);
fft_sig = fftshift(fft_sig);
fft_sig_f = fftshift(fft_sig_f);

plot(freq, mag2db(abs(fft_sig)));
hold on
plot(freq, mag2db(abs(fft_sig_f)));
hold off
xlim([0 max(freq)])
ylim([-20 60])
grid
set(gca, 'FontSize', 15)
title("Magnitude spectrum of white noise");
legend("Unfiltered", "Filtered with equalizer");
xlabel("Frequency [Hz]")
ylabel("Magnitude [dB]");
saveas(gcf, 'FrqRespWhiteNoise.eps', 'epsc');

% playerObj = audioplayer(sig_filtered,fs);
% play(playerObj);

%% My fav music
% [piano, fs_p] = audioread("spoken_sentence.wav");
% 
% piano_f = filter(b, a, piano);
% 
% playerObj = audioplayer(piano_f,fs_p);
% play(playerObj);
%% Stop it!!!!

%%stop(playerObj);
%x

%% Grandsma's DSP

fs = 5000;
T = 10; % [s]
t = 0:1/fs:T-1/fs;

sig = chirp(t, 50, T, 5000);

playerObj = audioplayer(sig,fs);
%%play(playerObj);

figure(7)
plot(abs(fft(sig)))


omega = 0:pi/400:10;
Average = (1/3)*(1-exp(-i*omega*3))./(1-exp(-i*omega));

figure(8);
plot(omega,[abs(Average)])
axis([0, 10, 0, 1])
xlabel("Hz")
ylabel(" normalized amplitude")
title("3-point filter magnitude plot");
saveas(gcf, '3PointFilter.eps', 'epsc');

figure(9);
fase=unwrap(angle(Average),1);
plot(omega,unwrap(fase))
% axis([0, 10, 0, 1])
xlabel("Hz")
ylabel("Phase(Degrees)")
title("3-point filter phase plot");
saveas(gcf, '3PointFilterPhase.eps', 'epsc');

