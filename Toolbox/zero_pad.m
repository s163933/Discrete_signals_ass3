function signal = zero_pad(input_signal, fs, T0)
%ZERO_PAD Summary of this function goes here
%   Detailed explanation goes here

signal = zeros(1,fs*T0 - length(input_signal));
signal = [input_signal, signal];

end

