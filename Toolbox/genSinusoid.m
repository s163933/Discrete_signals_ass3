function x = genSinusoid(fs,T,f0,A)
N=T*fs;
x=zeros(1,N);
t=0:1/fs:T-1/fs;

for n=1:N
    x(n)=x(n)+A*sin(2*pi*f0*t(n));
end