function [frequency_bins,magnitude,phase] = bodeFromImpulse(impulseResponse,fs)
%BODEFROMIMPULSE Bode plots from impulse responses
%   Input:
%       impulseResponse: A vector with the impulse response of the system.
%       fs: The sampling frequency used to generate the impulse response
%   Output:
%       frequency_bins: Vector of frequency bins, use as x-axis.
%       magnitude: The magnitude response, in dB, use as first y-axis.
%       phase: The phase response in degrees, use as second y-axis.

fft_impulse = fft(impulseResponse);

half_length = floor(length(impulseResponse)/2);

% t_filt = triple filtered
fft_abs = abs(fft_impulse);
fft_abs = fft_abs(1:1+half_length);
magnitude = 20*log10(fft_abs);

fft_phase = (angle(fft_impulse))*180/pi;
phase = fft_phase(1:1+half_length);

frequency_bins = (0:half_length) * fs / length(impulseResponse);

end

