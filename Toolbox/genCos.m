function [x,t] = genCos(fs,T,f0)
N=T*fs;
x=zeros(1,N);
t=0:1/fs:T-1/fs;

for n=1:N
    for k=0:4
        x(n)=x(n)+cos(2*pi*(2^k)*f0*t(n)+k*3/pi);
    end
end