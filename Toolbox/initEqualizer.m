function [b,a] = initEqualizer(gains)
%INITEQUALIZER Summary of this function goes here
%   gains -> vector of size 5, with gains of different filter bands
%   The bands are each 1/5 of the unit circle

divisor = 5;
order = 101;
% We use the function from hands on 8
% Do a neutral initial b
b = zeros(1,order);
b(1) = 1;
a = 1;
% k = 1;
% [b, a] = bassandtreb([(k-1)/divisor k/divisor], gains(k), order)
for k = 1:5
    [temp_b,temp_a] = bassandtreb([(k-1)/divisor k/divisor], gains(k), order);
    b = conv(b, temp_b);
end

end

