function [Y, freq] = make_spectrum(signal, fs)



Y = fft(signal);
Y=fftshift(Y);

Y = Y/(length(Y));


delta_f = fs/length(Y);
freq = -fs/2:delta_f: fs/2-delta_f;

Y = Y(:);
freq = freq(:);
% eof
end
