function [x, t] = genSin(fs, T, f0, A0)
if (length(A0) ~= length(f0))
    x = 0;
    return
end

N = fs*T;    
x = zeros(1,N);
t = 0:1/fs:T-1/fs;

for n = 1:N
    for i = 1:length(f0)
        x(n) = x(n) + A0(i)*sin(2*pi*t(n)*f0(i));
    end
end
